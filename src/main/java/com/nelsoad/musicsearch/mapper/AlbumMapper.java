package com.nelsoad.musicsearch.mapper;

import java.util.ArrayList;
import java.util.List;

import com.nelsoad.musicsearch.DTO.AlbumDTO;
import com.nelsoad.musicsearch.model.ITunesObject;
import com.nelsoad.musicsearch.model.ITunesResults;

public class AlbumMapper {
	
	public static AlbumDTO mapITunesObjectToAlbumDTO(ITunesObject itunesObject) {
		
		AlbumDTO album = new AlbumDTO();
		
		album.setArtist(itunesObject.getArtistName());
		album.setArtistId(itunesObject.getArtistId());
		album.setCollection(itunesObject.getCollectionName());
		album.setCollectionId(itunesObject.getCollectionId());
		album.setArtwork60(itunesObject.getArtworkUrl60());
		album.setArtwork100(itunesObject.getArtworkUrl100());
		
		return album;
	}
	
	public static List<AlbumDTO> mapITunesResultsToListOfAlbumDTOs(ITunesResults itunesResults) {
		
		ArrayList<AlbumDTO> albums = new ArrayList<AlbumDTO>();
		
		for(ITunesObject obj : itunesResults.getResults()) {
			AlbumDTO album = mapITunesObjectToAlbumDTO(obj);
			albums.add(album);
		}
		
		return albums;
	}
	
}
