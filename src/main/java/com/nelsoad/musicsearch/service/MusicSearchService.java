package com.nelsoad.musicsearch.service;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.nelsoad.musicsearch.model.ITunesResults;

@Service
public interface MusicSearchService {
	public ITunesResults findAlbumsByTerm(String term) throws IOException;
}
