package com.nelsoad.musicsearch.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ITunesObject {
	
	private String wrapperType;
	private String explicitness;
	private String kind;
	private String trackName;
	private String artistName;
	private String artistId;
	private String collectionName;
	private String collectionId;
	private String censoredName;
	private String artworkUrl100;
	private String artworkUrl60;
	private int trackTimeMillis;
	
	public ITunesObject() {
		
	}
	
	public String getWrapperType() {
		return wrapperType;
	}
	
	public void setWrapperType(String wrapperType) {
		this.wrapperType = wrapperType;
	}
	
	public String getExplicitness() {
		return explicitness;
	}
	
	public void setExplicitness(String explicitness) {
		this.explicitness = explicitness;
	}
	
	public String getKind() {
		return kind;
	}
	
	public void setKind(String kind) {
		this.kind = kind;
	}
	
	public String getTrackName() {
		return trackName;
	}
	
	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}
	
	public String getArtistName() {
		return artistName;
	}
	
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	public String getArtistId() {
		return artistId;
	}

	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	public String getCollectionName() {
		return collectionName;
	}
	
	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
	
	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getCensoredName() {
		return censoredName;
	}
	
	public void setCensoredName(String censoredName) {
		this.censoredName = censoredName;
	}
	
	public String getArtworkUrl100() {
		return artworkUrl100;
	}
	
	public void setArtworkUrl100(String artworkUrl100) {
		this.artworkUrl100 = artworkUrl100;
	}
	
	public String getArtworkUrl60() {
		return artworkUrl60;
	}
	
	public void setArtworkUrl60(String artworkUrl60) {
		this.artworkUrl60 = artworkUrl60;
	}
	
	public int getTrackTimeMillis() {
		return trackTimeMillis;
	}
	
	public void setTrackTimeMillis(int trackTimeMillis) {
		this.trackTimeMillis = trackTimeMillis;
	}
}
