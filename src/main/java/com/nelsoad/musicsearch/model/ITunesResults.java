package com.nelsoad.musicsearch.model;

import java.util.List;

public class ITunesResults {
	
	private int resultCount;
	private List<ITunesObject> results;
	
	public ITunesResults() {
		
	}
	
	public int getResultCount() {
		return resultCount;
	}
	
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}
	
	public List<ITunesObject> getResults() {
		return results;
	}
	
	public void setResults(List<ITunesObject> results) {
		this.results = results;
	}
}
