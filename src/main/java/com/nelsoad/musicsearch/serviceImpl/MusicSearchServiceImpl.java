package com.nelsoad.musicsearch.serviceImpl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nelsoad.musicsearch.model.ITunesObject;
import com.nelsoad.musicsearch.model.ITunesResults;
import com.nelsoad.musicsearch.service.MusicSearchService;

@Service
public class MusicSearchServiceImpl implements MusicSearchService {

	Logger log = LoggerFactory.getLogger(MusicSearchServiceImpl.class);
	
	@Override
	public ITunesResults findAlbumsByTerm(String term) throws IOException {
		
		//get json data for search results
		RestTemplate rest = new RestTemplate();
		String url = "https://itunes.apple.com/search?term=" + term + "&entity=album&limit=200";
		String jsonData = rest.getForObject(url, String.class);
		
		//convert json data into an ITunesResults object
		ObjectMapper objectMapper = new ObjectMapper();
		ITunesResults results = objectMapper.readValue(jsonData, ITunesResults.class);
		
		return results;
	}

}
