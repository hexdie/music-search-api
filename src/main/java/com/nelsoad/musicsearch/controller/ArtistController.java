package com.nelsoad.musicsearch.controller;

import java.util.List;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.nelsoad.musicsearch.DTO.AlbumDTO;
import com.nelsoad.musicsearch.DTO.SearchTermDTO;
import com.nelsoad.musicsearch.mapper.AlbumMapper;
import com.nelsoad.musicsearch.model.ITunesObject;
import com.nelsoad.musicsearch.model.ITunesResults;
import com.nelsoad.musicsearch.service.MusicSearchService;

@RestController
@RequestMapping(value = "/albums")
public class ArtistController {
	
	Logger log = LoggerFactory.getLogger(ArtistController.class);
	
	@Autowired
	MusicSearchService musicSearchService;
	
	@GetMapping(value = "/{value}", produces = "application/json")
	public List<AlbumDTO> getAlbums(SearchTermDTO term) {
		log.info(term.getValue());
		
		try {
			//Search ITunes and return a list of album
			ITunesResults results = musicSearchService.findAlbumsByTerm(term.getValue());
			List<AlbumDTO> albums = AlbumMapper.mapITunesResultsToListOfAlbumDTOs(results);
					
			return albums;
		}
		catch(IOException ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred connecting to remote API");
		}
	}
	
}
