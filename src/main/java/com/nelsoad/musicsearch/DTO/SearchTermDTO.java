package com.nelsoad.musicsearch.DTO;

public class SearchTermDTO {
	
	private String value;

	public SearchTermDTO() {
		
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
