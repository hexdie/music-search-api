package com.nelsoad.musicsearch.DTO;

public class AlbumDTO {
	
	public String artist;
	public String artistId;
	public String collection;
	public String collectionId;
	public String artwork100;
	public String artwork60;
	
	public String getArtist() {
		return artist;
	}
	
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public String getArtistId() {
		return artistId;
	}
	
	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}
	
	public String getCollection() {
		return collection;
	}
	
	public void setCollection(String collection) {
		this.collection = collection;
	}
	
	public String getCollectionId() {
		return collectionId;
	}
	
	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}
	
	public String getArtwork100() {
		return artwork100;
	}
	
	public void setArtwork100(String artwork100) {
		this.artwork100 = artwork100;
	}
	
	public String getArtwork60() {
		return artwork60;
	}
	
	public void setArtwork60(String artwork60) {
		this.artwork60 = artwork60;
	}
}
