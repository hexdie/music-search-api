# Music Album Search API

API for an music album search application

## Local Setup

1. This API was built targeting Java 1.8 (1.8.0_221)
2. Clone this repository locally
3. In the local repository directory
    - run `./gradlew bootRun`
    - (or `gradlew.bat bootRun` on Windows)
